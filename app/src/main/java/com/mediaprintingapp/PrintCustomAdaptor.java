package com.mediaprintingapp;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.RectF;
import android.graphics.pdf.PdfDocument;
import android.os.Bundle;
import android.os.CancellationSignal;
import android.os.Environment;
import android.os.ParcelFileDescriptor;
import android.print.PageRange;
import android.print.PrintAttributes;
import android.print.PrintDocumentAdapter;
import android.print.PrintDocumentInfo;
import android.print.pdf.PrintedPdfDocument;
import android.widget.Toast;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.nio.MappedByteBuffer;
import java.nio.channels.FileChannel;
import java.util.ArrayList;

public class PrintCustomAdaptor extends PrintDocumentAdapter
{
    PdfDocument pdfDocument;
    Context context;
    private int pageHeight;
    private int pageWidth;
    public PdfDocument myPdfDocument;
    public int totalpages = 0;
    ArrayList imageList;

    public PrintCustomAdaptor (Context context, ArrayList imgList)
    {
        this.context = context;
        this.totalpages=imgList.size();
        this.imageList=imgList;
    }

    @Override
    public void onLayout(PrintAttributes oldAttributes, PrintAttributes newAttributes, CancellationSignal cancellationSignal, LayoutResultCallback callback, Bundle extras) {
        // Create a new PdfDocument with the requested page attributes
        myPdfDocument = new PrintedPdfDocument(context, newAttributes);

        pageHeight =
                newAttributes.getMediaSize().getHeightMils()/1000 * 72;
        pageWidth =
                newAttributes.getMediaSize().getWidthMils()/1000 * 72;

        if (cancellationSignal.isCanceled() ) {
            callback.onLayoutCancelled();
            return;
        }

        if (totalpages > 0) {
            PrintDocumentInfo.Builder builder = new PrintDocumentInfo
                    .Builder("print_output.pdf")
                    .setContentType(PrintDocumentInfo.CONTENT_TYPE_DOCUMENT)
                    .setPageCount(totalpages);

            PrintDocumentInfo info = builder.build();
            callback.onLayoutFinished(info, true);
        } else {
            callback.onLayoutFailed("Page count is zero.");
        }
    }

    @Override
    public void onWrite(PageRange[] pages, ParcelFileDescriptor destination, CancellationSignal cancellationSignal, WriteResultCallback callback) {
        for (int i = 0; i < totalpages; i++) {
            if (pageInRange(pages, i))
            {
                PdfDocument.PageInfo newPage = new PdfDocument.PageInfo.Builder(pageWidth,
                        pageHeight, i).create();

                PdfDocument.Page page =
                        myPdfDocument.startPage(newPage);

                if (cancellationSignal.isCanceled()) {
                    callback.onWriteCancelled();
                    myPdfDocument.close();
                    myPdfDocument = null;
                    return;
                }

                Canvas canvas = page.getCanvas();

               Bitmap bitmap2 = (Bitmap)imageList.get(i);

               if(bitmap2.getWidth()>=bitmap2.getHeight()) {
                   double a = canvas.getWidth();
                   double b = bitmap2.getWidth();

                   double ar_width = a / b;

                   double f_height = 0, f_width = 0;

                   f_width = bitmap2.getWidth() * ar_width;
                   f_height = bitmap2.getHeight() * ar_width;

            //       Toast.makeText(context, ar_width + " " + f_height + " " + f_width + " " + bitmap2.getWidth() + " " + canvas.getWidth(), Toast.LENGTH_SHORT).show();

//               Bitmap bitmap3=bitmap2.copy(Bitmap.Config.ARGB_8888, true);
//
//               bitmap3.setHeight(canvas.getHeight());
//               bitmap3.setWidth(canvas.getWidth());
                   RectF dst = new RectF(30, 30, (int) f_width, (int) f_height);
                   canvas.drawBitmap(
                           bitmap2, // Bitmap
                           null, // Left
                           dst, // Top
                           null // Paint
                   );

               }
               else
               {
                   double a = canvas.getHeight();
                   double b = bitmap2.getHeight();

                   double ar_width = a / b;

                   double f_height = 0, f_width = 0;

                   f_width = bitmap2.getWidth() * ar_width;
                   f_height = bitmap2.getHeight() * ar_width;

                   Toast.makeText(context, ar_width + " " + f_height + " " + f_width + " " + bitmap2.getWidth() + " " + canvas.getWidth(), Toast.LENGTH_SHORT).show();

//               Bitmap bitmap3=bitmap2.copy(Bitmap.Config.ARGB_8888, true);
//
//               bitmap3.setHeight(canvas.getHeight());
//               bitmap3.setWidth(canvas.getWidth());
                   RectF dst = new RectF(30, 30, (int) f_width, (int) f_height);
                   canvas.drawBitmap(
                           bitmap2, // Bitmap
                           null, // Left
                           dst, // Top
                           null // Paint
                   );
               }

                // drawPage(page, i);
                myPdfDocument.finishPage(page);
            }
        }

        try {
            myPdfDocument.writeTo(new FileOutputStream(
                    destination.getFileDescriptor()));
        } catch (IOException e) {
            callback.onWriteFailed(e.toString());
            return;
        } finally {
            myPdfDocument.close();
            myPdfDocument = null;
        }

        callback.onWriteFinished(pages);
    }

    private boolean pageInRange(PageRange[] pageRanges, int page)
    {
        for (int i = 0; i<pageRanges.length; i++)
        {
            if ((page >= pageRanges[i].getStart()) &&
                    (page <= pageRanges[i].getEnd()))
                return true;
        }
        return false;
    }


    public  Bitmap convertToMutable(Bitmap imgIn) {
        try {
            //this is the file going to use temporally to save the bytes.
            // This file will not be a image, it will store the raw image data.
            File file = new File(Environment.getExternalStorageDirectory() + File.separator + "temp.tmp");

            //Open an RandomAccessFile
            //Make sure you have added uses-permission android:name="android.permission.WRITE_EXTERNAL_STORAGE"
            //into AndroidManifest.xml file
            RandomAccessFile randomAccessFile = new RandomAccessFile(file, "rw");

            // get the width and height of the source bitmap.
            int width = imgIn.getWidth();
            int height = imgIn.getHeight();
            Bitmap.Config type = imgIn.getConfig();

            //Copy the byte to the file
            //Assume source bitmap loaded using options.inPreferredConfig = Config.ARGB_8888;
            FileChannel channel = randomAccessFile.getChannel();
            MappedByteBuffer map = channel.map(FileChannel.MapMode.READ_WRITE, 0, imgIn.getRowBytes()*height);
            imgIn.copyPixelsToBuffer(map);
            //recycle the source bitmap, this will be no longer used.
            imgIn.recycle();
            System.gc();// try to force the bytes from the imgIn to be released

            //Create a new bitmap to load the bitmap again. Probably the memory will be available.
            imgIn = Bitmap.createBitmap(width, height, type);
            map.position(0);
            //load it back from temporary
            imgIn.copyPixelsFromBuffer(map);
            //close the temporary file and channel , then delete that also
            channel.close();
            randomAccessFile.close();

            // delete the temp file
            file.delete();

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        return imgIn;
    }

    public double computeImageDim(Bitmap bitmap)
    {
        double height=bitmap.getHeight();
        double width=bitmap.getWidth();

        double ar=width/height;

        return ar;

    }

}
