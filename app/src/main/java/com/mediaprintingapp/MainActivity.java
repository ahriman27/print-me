package com.mediaprintingapp;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.print.PrintHelper;

import android.Manifest;
import android.app.Activity;
import android.content.ClipData;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.print.PrintManager;
import android.provider.MediaStore;
import android.util.Log;
import android.view.View;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Toast;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {
    private static final int PICK_IMAGE = 100;
    Uri imageUri;
    int BitmapSize = 30;
    private static final int REQUEST_EXTERNAL_STORAGE = 1;
    private static String[] PERMISSIONS_STORAGE = {
            Manifest.permission.READ_EXTERNAL_STORAGE,
            Manifest.permission.WRITE_EXTERNAL_STORAGE
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        getSupportActionBar().hide();

        verifyStoragePermissions((Activity)MainActivity.this);

    }

    public void openGallery(View v) {
        Intent intent = new Intent();
        intent.setType("image/*");
        intent.putExtra(Intent.EXTRA_ALLOW_MULTIPLE, true);
        intent.setAction(Intent.ACTION_GET_CONTENT);
        startActivityForResult(Intent.createChooser(intent,"Select Picture"), PICK_IMAGE);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data){
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK && requestCode == PICK_IMAGE){

            ArrayList imageurilist=new ArrayList();

            if(data.getData()!=null)
            {
                imageUri = data.getData();
                try {
                    Bitmap bitmap = MediaStore.Images.Media.getBitmap(getContentResolver(),imageUri);
                    imageurilist.add(bitmap);
                }
               catch (Exception e)
               {

               }
            }
            else if(data.getClipData()!=null)
            {
                ClipData clipData=data.getClipData();

                for(int i=0;i<clipData.getItemCount();i++)
                {
                    ClipData.Item item=clipData.getItemAt(i);
                    try {
                        Bitmap bitmap = MediaStore.Images.Media.getBitmap(getContentResolver(), item.getUri());
                        imageurilist.add(bitmap);
                    } catch (Exception e) {
                        Toast.makeText(getApplicationContext(), "Print Unsuccessful " + e, Toast.LENGTH_SHORT).show();
                    }

                }

            }
            else
            {
                Toast.makeText(getApplicationContext(), "You didn't select an image", Toast.LENGTH_SHORT).show();
            }

            PrintManager printManager = (PrintManager) this
                    .getSystemService(Context.PRINT_SERVICE);

            String jobName = this.getString(R.string.app_name) +
                    " Document";

            printManager.print(jobName, new PrintCustomAdaptor(getApplicationContext(),imageurilist),
                    null);

           // imageView.setImageURI(imageUri);
         //   doPhotoPrint(imageurilist);
        }
    }

    public void doPhotoPrint(ArrayList arrayList) {
        ArrayList<Bitmap> a=new ArrayList();
        PrintManager printManager=(PrintManager) getSystemService(Context.PRINT_SERVICE);

        PrintHelper photoPrinter = new PrintHelper(MainActivity.this);
        photoPrinter.setScaleMode(PrintHelper.SCALE_MODE_FIT);
        for(int j=0;j<arrayList.size();j++) {

            try {
                Bitmap bitmap = MediaStore.Images.Media.getBitmap(getContentResolver(), (Uri)arrayList.get(j));
                photoPrinter.printBitmap("Job "+(j+1), bitmap);
            } catch (Exception e) {
                Toast.makeText(getApplicationContext(), "Print Unsuccessful " + e, Toast.LENGTH_SHORT).show();
            }

        }

    }

    public void webPage(View v)
    {
        Intent i=new Intent(getApplicationContext(),WebPrintActivity.class);
        startActivity(i);
    }

    public void pdfprint(View v)
    {
        Intent i=new Intent(getApplicationContext(),PdfRenderActivity.class);
        startActivity(i);
    }

    public static void verifyStoragePermissions(Activity activity) {
        // Check if we have write permission
        int permission = ActivityCompat.checkSelfPermission(activity, Manifest.permission.WRITE_EXTERNAL_STORAGE);

        if (permission != PackageManager.PERMISSION_GRANTED) {
            // We don't have permission so prompt the user
            ActivityCompat.requestPermissions(
                    activity,
                    PERMISSIONS_STORAGE,
                    REQUEST_EXTERNAL_STORAGE
            );
        }
    }

    public int GetBitmapWidth(Bitmap bitmap1){

        int width = bitmap1.getWidth() + BitmapSize * 2;
        return width;

    }

    public int GetBitmapHeight(Bitmap bitmap1){


       int height = bitmap1.getHeight() + BitmapSize * 2;
return height;
    }

}