package com.mediaprintingapp;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

public class WebPrintActivity extends AppCompatActivity {
EditText editText;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_web_print);

        getSupportActionBar().hide();

        editText=findViewById(R.id.webedittext);

    }

    public void print(View v)
    {
        String s=editText.getText().toString();

        if(s.length()>5) {
            Intent i = new Intent(getApplicationContext(), ShowUrl.class);
            i.putExtra("url", s);
            startActivity(i);
        }
        else
        {
            Toast.makeText(getApplicationContext(), "Please enter valid Url", Toast.LENGTH_SHORT).show();
        }

    }

}